package controllers;

import javafx.fxml.FXML;

/**
 * This class control login.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
import javafx.scene.control.TextField;
import photostorage.PhotoStorage;
import storage.User;

import java.util.List;
public class LoginController {
	 private List<User> users;
	 private PhotoStorage photoStorage;
	 @FXML
	 private TextField userName;
	 
	 /**
	  * 
	  * Login with input user name
	  * If the user name is an admin, go to admin page
	  * If the user name is a user, go to User Album page
	  * 
	  */
	 @FXML
	 private void login() {
		 String username = userName.textProperty().get();
		 boolean contains = false;
		 for(User user: users) {
			 if(user.getUserName().equals(username)) {
				 contains = true;
			 }
		 }
		 if(contains) {
			 if(username.equals("admin")) {
				 this.photoStorage.pageAdmin();
			 } else {
				 this.photoStorage.pageUserAlbums(username);
			 }
		 } else {
			ErrorMsg.Error("ERROR!", "User does not exist!", 
					"User does not exist, please login with admin to add new users.");
		 }
	 }
		
	 /**
	  * Method to remove user
	  * 
	  */
	 @FXML
	 private void removeUserInfo() {
	 	this.photoStorage.removeAllPersistentFiles();
	 }
	 
	 /**
	  * Set to the main photoStorage
	  * 
	  * @param photoStorage The photo storage
	  */
	 public void setMainApp(PhotoStorage photoStorage) {
		 this.users = photoStorage.getUsers();
	 	 this.photoStorage = photoStorage;
	 	 this.photoStorage.getPrimaryStage().setTitle("login");
	 }
}
