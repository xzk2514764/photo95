package controllers;

import javafx.fxml.FXML;
import javafx.stage.Stage;

/**
 * This class control ChooseSearchCriteria.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class ChooseSearchCriteriaController {
	private Stage stage;
	private UserSearchController controller;
	
	/**
	 * Cancel this action and then go to User Search page
	 *   
	 */
	@FXML
	private void cancel() {
		stage.close();
	}
	
	/**
	 * Choose to search only by date
	 *   
	 */
	@FXML
	private void date() {
		stage.close();
		controller.searchWithDate();
	}
	
	/**
	 * Choose to search only by tag
	 *   
	 */
	@FXML
	private void tag() {
		stage.close();
		controller.searchWithTags();
	}
	
	/**
	 * Set to its father stage
	 * 
	 * @param window Stage
	 * @param c The User Search Controller
	 */
	public void setMainStage(Stage window, UserSearchController c) {
		stage = window;
		controller = c;
		
	}
}
