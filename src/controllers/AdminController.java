package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import photostorage.PhotoStorage;
import storage.User;
import java.util.Date;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

/**
 * This class control admin.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class AdminController {
	private PhotoStorage photoStorage;
	@FXML
    private TableView<User> userViews;
	@FXML
    private TableColumn<User, String> userCols;
	@FXML
	private TextField userName;
	@FXML
	private ChoiceBox<Date> rebaseVersion;
	
	/**
	 * Initialize this fxml stage
	 *   
	 */
	@FXML
	private void initialize() {
		userCols.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUserName()));
	}
	
	/**
	 * Add user whose user name is the text of input
	 *   
	 */
	@FXML
	private void addUser() {
		String username = userName.textProperty().get().strip();
		boolean contains = false;
		for(User user:this.photoStorage.getUsers()) {
			if(user.getUserName().equals(username)) {
				contains = true;
			}
		}
		if(contains) {
			ErrorMsg.Error("ERROR!", "Username already exists!", 
					"Please change to another username and try again!");
		} else if ( username.equals("")) {
			ErrorMsg.Error("ERROR!", "Username cannot be empty!", 
					"Please change to another username and try again!");
		} else {
			this.photoStorage.getModifiableUsers().add(new User(username));
			this.userViews.getSelectionModel().select(this.photoStorage.getUsers().size() - 1);
			userName.clear();
		}
	}
	
	/**
	 * Delete the selected user
	 *   
	 */
	@FXML
	private void delete() {
		int selectedIndex = userViews.getSelectionModel().getSelectedIndex();
    	if (selectedIndex == 0 ) {
    		ErrorMsg.Error("ERROR!", "Cannot delete admin!", "You cannot delete yourself!");
    		return;
    	} else {
    		this.photoStorage.getModifiableUsers().remove(selectedIndex);
    		userViews.getSelectionModel().select(selectedIndex - 1);
    	}
	}
	
	/**
	 * Rebase a history version
	 *   
	 */
	@FXML
	private void rebase() {
		if(this.rebaseVersion.getValue() == null) {
			ErrorMsg.Error("ERROR!", "No rebase version chosen!", "Please choose a previous version!");
			return;
		}
		this.photoStorage.rebaseVersion(this.rebaseVersion.getValue().getTime());
		userViews.setItems(photoStorage.getModifiableUsers());
	    userViews.getSelectionModel().select(0);
	}
	
	/**
	 * Reset this program
	 *   
	 */
	@FXML
	private void reset() {
		this.photoStorage.resetProgram();
		userViews.setItems(photoStorage.getModifiableUsers());
	    userViews.getSelectionModel().select(0);
	}
	
	/**
	 * Logout and then go to login page
	 *   
	 */
	@FXML
	private void logout() {
		this.photoStorage.pageLogin();
	}
	
	/**
	 * Confirm to rebase a version
	 *   
	 */
	@FXML
	private void onClick() {
		rebaseVersion.setItems(FXCollections.observableList(photoStorage.getAllVersion()));
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
	       this.photoStorage = photoStorage;
	       this.photoStorage.getPrimaryStage().setTitle("Manage Users.");
	       userViews.setItems(photoStorage.getModifiableUsers());
	       userViews.getSelectionModel().select(0);
	       userName.clear();
	       onClick();
	}	 
}
