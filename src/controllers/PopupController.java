package controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import javafx.beans.property.SimpleStringProperty;

/**
 * This class control popup.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class PopupController {
	private ObservableList<String> values;
	private Stage curStage;
	private SimpleStringProperty s;
	@FXML
	ChoiceBox<String> cb;
	
	/**
	 * Confirm to delete the selected tag value
	 * 
	 */
	@FXML
	private void OK() {
		if(cb.getValue()==null) {
			ErrorMsg.Error("ERROR!", "Please Choose a Value to Delete!", 
					"Please Choose a Value to Delete!");
			return;
		}
		values.remove(cb.getValue());
		s.set(values.toString());
		curStage.close();
	}
	
	/**
	 * Cancel to delete the tag value
	 * 
	 */
	@FXML
	private void cancel() {
		curStage.close();
	}
	
	/**
	 * Set to its father stage
	 * 
	 * @param values The tag values
	 * @param stage The father stage
	 * @param index The index of tag
	 * @param s SimpleStringProperty
	 */
	public void setMainStage( ObservableList<String> values, Stage stage, int index, SimpleStringProperty s) {
		cb.setItems(values);
		curStage = stage;
		this.values = values;
		this.s = s;
	}

}
