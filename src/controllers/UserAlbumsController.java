package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import photostorage.PhotoStorage;
import javafx.beans.property.SimpleStringProperty;
import java.util.Map;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import storage.Album;
import storage.Photo;

/**
 * This class control User Ablums.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class UserAlbumsController {
	private PhotoStorage photoStorage;
	private ObservableList<Album> albums;
	private Map<Album, SimpleStringProperty> albumNameMap = new HashMap<Album, SimpleStringProperty>();
	@FXML
    private TableView<Album> albumViews;
	@FXML
    private TableColumn<Album, String> albumCols;
	@FXML
	private TextField albumName;
	@FXML
	private TextField renameAlbumName;	
	@FXML
	private Label text1;
	@FXML
	private Label text2;
	@FXML
	private Label text3;
	@FXML
	private Label text4;
	
	/**
	 * Initialize this fxml stage
	 *   
	 */
	@FXML
	private void initialize() {
		albumCols.setCellValueFactory(cellData -> albumNameMap.get(cellData.getValue()));
		albumViews.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showAlbumName(newValue));
		albumViews.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showAlbumName(newValue));
	}
	
	/**
	 * Display the album name to the renameAlbumName TextField
	 *   
	 */
	private void showAlbumName(Album newValue) {
		if(this.albums.size() != 0) {
			long startTime = 0;
			long endTime = 0;
			renameAlbumName.setText(newValue.getAlbumName());
		    this.text1.setText(newValue.getAlbumName());
		    for(int i = 0; i < newValue.getPhotos().size(); i++) {
		    	List<Photo> photos = newValue.getPhotos();
		    	long time = new File(photos.get(i).getPath()).lastModified();	
		    	if(i == 0) {
		    		endTime = time;
		    		startTime = time;
		    	}
		    	if(time > endTime) {
		    		endTime = time;
		    	}else if(time < startTime){
		    		startTime = time;
		    	}
		    }
		    Date start = new Date(startTime);
		    Date end = new Date(endTime);
		    this.text2.setText(""+start);
		    this.text3.setText(""+end);
		    this.text4.setText(""+newValue.getPhotos().size());
		}
	}
	
	/**
	 * Add a new album with the input name
	 * One user cannot have two albums with the same name 
	 */
	@FXML
	private void addAlbum() {
		String albumName = this.albumName.textProperty().get().strip();
		boolean contains = false;
		for(Album album:albums) {
			if(album.getAlbumName().equals(albumName)) {
				contains = true;
			}
		}
		if(contains) {
			ErrorMsg.Error("ERROR!", "Album Name already exists for this user!", 
					"Please change to another username and try again!");
		} else if ( albumName.equals("")) {
			ErrorMsg.Error("ERROR!", "Album Name cannot be empty!", 
					"Please change to another username and try again!");
		} else {
			Album newAlbum = new Album(albumName);
			this.albums.add(newAlbum);
			this.albumNameMap.put(newAlbum, new SimpleStringProperty(newAlbum.getAlbumName()));
			this.albumViews.getSelectionModel().select(albums.size() - 1);
			this.albumName.clear();
		}
	}
	
	/**
	 * Delete the selected album
	 *   
	 */
	@FXML
	private void delete() {
		int selectedIndex = albumViews.getSelectionModel().getSelectedIndex();
    	this.albums.remove(selectedIndex);
    	if (selectedIndex != 0) {
    		albumViews.getSelectionModel().select(selectedIndex - 1);
    	}
	}
	
	/**
	 * Rename the selected album with the input name
	 *   
	 */
	@FXML
	private void rename() {
		if(albums.size() == 0) {
			ErrorMsg.Error("ERROR!", "Please select an album to rename!", 
					"You cannot rename the album when there is no album!");
			return;
		}
		String prevName = albums.get(albumViews.getSelectionModel().getSelectedIndex()).getAlbumName();
		String albumName = this.renameAlbumName.textProperty().get().strip();
		boolean contains = false;
		if (prevName.equals(albumName)) {
			return;
		}
		for(Album album:albums) {
			if(album.getAlbumName().equals(albumName)) {
				contains = true;
			}
		}
		if(contains) {
			ErrorMsg.Error("ERROR!", "Album Name already exists for this user!", 
					"Please change to another username and try again!");
		} else if ( albumName.equals("")) {
			ErrorMsg.Error("ERROR!", "Album Name cannot be empty!", 
					"Please change to another username and try again!");
		} else {
			this.albums.get(albumViews.getSelectionModel().getSelectedIndex()).changeAlbumName(albumName);
			albumNameMap.get(this.albums.get(albumViews.getSelectionModel().getSelectedIndex())).set(albumName);
		}
	}
	
	/**
	 * Logout and go to the login page
	 *   
	 */
	@FXML
	private void logout() {
		this.photoStorage.pageLogin();
	}
	
	/**
	 * Display the selected album and go to the Album View page
	 *   
	 */
	@FXML
	private void selectAlbum() {
		if(albums.size() == 0) {
			return;
		}
		Album album = albums.get((albumViews.getSelectionModel().getSelectedIndex()));
		this.photoStorage.pageAlbumView(album);
	}
	
	/**
	 * Go to User Search page to search photos
	 *   
	 */
	@FXML
	private void search() {
		this.photoStorage.pageUserSearch();
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
		this.photoStorage = photoStorage;
	    albums = FXCollections.observableList(photoStorage.getUser().getAlbums());
	    albumViews.setItems(albums);
	    for(Album album: albums) {
	    	albumNameMap.put(album, new SimpleStringProperty(album.getAlbumName()));
	    }
	    albumViews.getSelectionModel().select(0);
	    albumName.clear();
		this.photoStorage.getPrimaryStage().setTitle("Username: " + this.photoStorage.getUser().getUserName());
	}	 
}
