package controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import photostorage.PhotoStorage;
import storage.Album;
import storage.Photo;

/**
 * This class control User Search.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class UserSearchController {
	
	private PhotoStorage photoStorage;
	private boolean logicAnd = false;
	private boolean logicOr = false;
	private String key1;
	private String value1;
	private String key2;
	private String value2;
	private Album searchResult;
	
	@FXML
	ChoiceBox<String> keyInput;
	
	@FXML
	TextField valueInput;
	
	@FXML
	DatePicker startDate;
	
	@FXML
	DatePicker endDate;
	
	@FXML
	Label curSearchCriteria;
	
	/**
	 * To search with condition that tag and tag
	 * 
	 */
	@FXML
	private void and() {
		if(key1 == null) {
			ErrorMsg.Error("ERROR!", "You do not have any search criteria to use <and> now!", 
					"Please add the first searching criteria!");
			return;
		}
		if(key2!=null) {
			ErrorMsg.Error("ERROR!", "You cannot add more than two searching criteria!", 
					"Please clear all the searching criterias and try again!");
			return;
		}
		logicAnd = true;
		logicOr = false;
		curSearchCriteria.setText(key1 + "=" + value1 + " && ");
	}
	
	/**
	 * To search with condition that tag or tag
	 * 
	 */
	@FXML
	private void or() {
		if(key1 == null) {
			ErrorMsg.Error("ERROR!", "You do not have any search criteria to use <and> now!", 
					"Please add the first searching criteria!");
			return;
		}
		if(key2!=null) {
			ErrorMsg.Error("ERROR!", "You cannot add more than two searching criteria!", 
					"Please clear all the searching criterias and try again!");
			return;
		}
		logicAnd = false;
		logicOr = true;
		curSearchCriteria.setText(key1 + "=" + value1 + " || ");
	}

	/**
	 * Confirm to add one tag and one tag value to search
	 * 
	 */
	@FXML
	private void confirm() {
		String key =  keyInput.getValue();
		String value = valueInput.getText();
		if (key2 != null) {
			ErrorMsg.Error("ERROR!", "You cannot add more than two searching criteria!", 
					"Please clear all the searching criterias and try again!");
			return;
		}
		if(key == null || value == null) {
			ErrorMsg.Error("ERROR!", "You cannot confirm with empty searching criteria!", 
					"Please make sure you choose the name of the tag and enter its value!");
			return;
		}
		
		if (key1 == null) {
			key1 = key;
			value1 = value;
			curSearchCriteria.setText(key1 + "=" + value1);
		} else {
			if((!logicAnd) && (!logicOr)) {
				ErrorMsg.Error("ERROR!", "You cannot confirm the second criteria without a logic connection!", 
						"Please choose between and/or before confirm it!");
				return;
			}
			key2 = key;
			value2 = value;
			String logic;
			if(logicAnd) {
				logic = " && ";
			} else {
				logic = " || ";
			}
			curSearchCriteria.setText(key1 + "=" + value1 + logic + key2 + "=" + value2);
		}
		valueInput.clear();
	}
	
	/**
	 * Clear all input of tags and tag values
	 * 
	 */
	@FXML
	private void clear() {
		key1 = null;
		value1 = null;
		key2 = null;
		value2 = null;
		logicAnd = false;
		logicOr = false;
		curSearchCriteria.setText("");
		valueInput.clear();
	}
	
	/**
	 * Confirm to search with given condition
	 * Date or Tags
	 * 
	 */
	@FXML
	private void search() {
		LocalDate start = startDate.getValue();
		LocalDate end = endDate.getValue();
		boolean useDate = false;
		boolean useTag = false;
		if (start != null || end != null) {
			useDate = true;	
		}
		
		if(key1 != null) {
			useTag = true;
		}
		
		if((!useDate) && (!useTag)) {
			ErrorMsg.Error("ERROR!", "You cannot search without a criteria!", 
					"Please add the date or tag key/value pairs!");
			return;
		}
		
		if(useDate && (!useTag)) {
			if(start!=null && end !=null && start.compareTo(end) > 0 ) {
				ErrorMsg.Error("ERROR!", "You cannot search with startDate later than endDate!", 
						"Please change the dates!");
				return;
			}
			searchWithDate();
		}
		
		if(useTag && (!useDate)) {
			searchWithTags();
		}
		
		if(useTag && useDate) {
			Stage newWindow = new Stage();
			newWindow.initModality(Modality.WINDOW_MODAL);
			newWindow.initOwner(this.photoStorage.getPrimaryStage());
			this.photoStorage.addSubStage(newWindow);
			try {
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(getClass().getResource("../photostorage/ChooseSearchCriteria.fxml"));
	            AnchorPane layout = (AnchorPane) loader.load();

	            newWindow.setScene(new Scene(layout));
	            ChooseSearchCriteriaController controller = loader.getController();
	            controller.setMainStage(newWindow, this);
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        newWindow.setX(this.photoStorage.getPrimaryStage().getX());
	        newWindow.setY(this.photoStorage.getPrimaryStage().getY());

	        newWindow.show();
		}
		
	
	}
	
	/**
	 * Search with Date
	 * 
	 */
	public void searchWithDate() {
		
		long start = 0; 
		if(startDate.getValue()!=null) {
			start = startDate.getValue().atStartOfDay(ZoneId.of("America/New_York")).toEpochSecond();
		}
		
		long end = 0;
		if(endDate.getValue()!=null) {
			end = endDate.getValue().plusDays(1).atStartOfDay(ZoneId.of("America/New_York")).toEpochSecond();
		}
		searchResult = new Album("search result");
		for(Album album: this.photoStorage.getUser().getAlbums()) {
			for(Photo photo: album.getPhotos()) {
				if(end == 0) {
					end = photo.getLastModifiedTime() + 1;
				}
				if(start <= photo.getLastModifiedTime() && photo.getLastModifiedTime() <= end ) {
					if(!searchResult.getPhotos().contains(photo)) {
						searchResult.getPhotos().add(photo);
					}
				}
			}
		}
		chooseCreateAlbumOrNot();
	}
	
	/**
	 * Open a new window to make sure create a new album with search results or not
	 * This new window is SearchCreateAlbum page
	 * 
	 */
	private void chooseCreateAlbumOrNot() {
		Stage newWindow = new Stage();
		newWindow.initModality(Modality.WINDOW_MODAL);
		newWindow.initOwner(this.photoStorage.getPrimaryStage());
		this.photoStorage.addSubStage(newWindow);
		try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../photostorage/SearchCreateAlbum.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            newWindow.setScene(new Scene(layout));
            SearchCreateAlbumController controller = loader.getController();
            controller.setMainStage(newWindow, this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        newWindow.setX(this.photoStorage.getPrimaryStage().getX());
        newWindow.setY(this.photoStorage.getPrimaryStage().getY());

        newWindow.show();
	}
	
	/**
	 * In SearchCreateAlbum page, set the new album name with input name
	 * 
	 * @param name The new album name
	 * @return True if set success, false if not
	 */
	public boolean setAlbumName(String name) {
		for(Album album: this.photoStorage.getUser().getAlbums()) {
			if(name.equals(album.getAlbumName())) {
				return false;
			}
		}
		this.searchResult.changeAlbumName(name);
		return true;
	}
	
	/**
	 * In SearchCreateAlbum page set, choose to create a new album
	 * 
	 */
	public void createAlbum() {
		this.photoStorage.getUser().getAlbums().add(this.searchResult);
		this.photoStorage.pageAlbumView(this.searchResult);
	}
	
	/**
	 * In SearchCreateAlbum page set, choose not to create a new album
	 * 
	 */
	public void noCreateAlbum() {
		this.photoStorage.pageAlbumView(this.searchResult);
	}
	
	/**
	 * Search with Tags
	 * 
	 */
	public void searchWithTags() {
		searchResult = new Album("search result");
		for(Album album: this.photoStorage.getUser().getAlbums()) {
			for(Photo photo: album.getPhotos()) {
				if(key2 == null) {
					if(photo.hasTag(key1, value1)) {
						if(!searchResult.getPhotos().contains(photo)) {
							searchResult.getPhotos().add(photo);
						}
					}
				} else {
					if(logicAnd) {
						if(photo.hasTag(key1, value1) && photo.hasTag(key2, value2)) {
							if(!searchResult.getPhotos().contains(photo)) {
								searchResult.getPhotos().add(photo);
							}
						}
					} else {
						if(photo.hasTag(key1, value1) || photo.hasTag(key2, value2)) {
							if(!searchResult.getPhotos().contains(photo)) {
								searchResult.getPhotos().add(photo);
							}
						}
					}
				}
			}
		}
		chooseCreateAlbumOrNot();
	}
	
	/**
	 * Stop searching and go back to the User Album page
	 * 
	 */
	@FXML
	private void back() {
		this.photoStorage.pageUserAlbums();
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
		this.photoStorage = photoStorage;
		this.keyInput.setItems(FXCollections.observableList(photoStorage.getUser().getAllTagKeyTypes()));
	}
}
