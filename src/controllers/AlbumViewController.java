package controllers;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import photostorage.PhotoStorage;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import storage.Photo;

/**
 * This class control Album View.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class AlbumViewController {
	private PhotoStorage photoStorage;
	private ObservableList<Photo> photos;
	private List<Pane> panes = new ArrayList<Pane>();
	private Photo curPhoto;
	private Label curLabel;
	@FXML 
	GridPane gridpane;
	@FXML
	ScrollPane scrollpane;
	@FXML
	private TextField renameCaption;
	
	/**
	 * Set the grid pane of photo in albums
	 *   
	 */
	private void setGridPane(int startIndex) {
	    int col = startIndex % 3;
	    int row = startIndex/3;
	    
	    this.gridpane.getChildren().removeIf(node -> 3* GridPane.getRowIndex(node) + GridPane.getColumnIndex(node) >= startIndex);
	    ImageView curPhoto;
	    File file;
	    this.panes = this.panes.subList(0,  startIndex);
		for(int i = startIndex; i < this.photos.size(); i ++) {
			Photo photo = this.photos.get(i);
			file = new File(photo.getPath());
	    	Pane pane = new Pane();
	    	panes.add(pane);
		    pane.setPrefSize(96,116);

			curPhoto = new ImageView(new Image(file.toURI().toString(), 90, 90, false, false));
	    	pane.setOnMouseClicked(e -> {
                Pane curPane = (Pane) e.getSource();
                for(Pane otherpane:panes) {
                	otherpane.setStyle("-fx-background-color: white;");
                	Label label = (Label)otherpane.getChildren().get(1);
                    label.setTextFill(Color.web("#535455"));
                }
                curPane.setStyle("-fx-background-color: #818399;");
                Label label = (Label)curPane.getChildren().get(1);
                label.setTextFill(Color.web("#e3e6f8"));
                this.curPhoto = photos.get(panes.indexOf(curPane));
                this.curLabel = label;
                this.renameCaption.setText(this.curPhoto.getCaption());

            });
	    	pane.getChildren().add(curPhoto);
	    	Label label = new Label();
	    	label.setPrefSize(96, 20);
	    	label.setText(photo.getCaption());
	    	label.setFont(new Font("Times New Roman", 13));
	    	label.setAlignment(Pos.CENTER);
	    	label.setTextAlignment(TextAlignment.CENTER);

	    	pane.getChildren().add(label);
	    	curPhoto.setX(3);
	    	curPhoto.setY(3);
	    	label.setLayoutX(0);
	    	label.setLayoutY(96);
	    	
	    	gridpane.add(pane, col, row);
	    	col += 1;
	    	if (col == 3) {
	    		row += 1;
	    		col = 0;
	    	}
	    }
	}
	
	/**
	 * Initialize this fxml stage
	 *   
	 */
	@FXML
	private void initialize() {
		this.gridpane = new GridPane();
	    scrollpane.setContent(this.gridpane);
	}
	
	/**
	 * Logout and then go to login page
	 *   
	 */
	@FXML
	private void logout() {
		this.photoStorage.pageLogin();
	}
	
	/**
	 * Back and then go to User Albums page
	 *   
	 */
	@FXML
	private void back() {
		this.photoStorage.pageUserAlbums();
	}
	
	/**
	 * Add image to current album
	 *   
	 */
	@FXML
	private void addImage() {
		FileChooser fileChooser = new FileChooser();
		int startIndex = this.photos.size();
		List<File> list =  fileChooser.showOpenMultipleDialog(this.photoStorage.getPrimaryStage());
        if (list != null) {
        	List<Photo> photos = new ArrayList<Photo>();
            for (File file : list) {
              	Photo newPhoto = new Photo(file.getAbsolutePath());
              	if(newPhoto.isValid()) {
              		photos.add(newPhoto);
              	}
            }
            this.photos.addAll(photos);
        }
        setGridPane(startIndex);
	}

	/**
	 * Delete the selected image from current album
	 *   
	 */
	@FXML
	private void deleteImage() {
		int startIndex = this.photos.indexOf(this.curPhoto);
		this.photos.remove(this.curPhoto);
		setGridPane(startIndex);
	}
	
	/**
	 * Recaption the selected image
	 *   
	 */
	@FXML
	private void recaptionImage() {
		this.curPhoto.setCaption(this.renameCaption.getText());
		this.curLabel.setText(this.renameCaption.getText());
	}
	
	/**
	 * Display the selected image and go to Photo View page
	 *   
	 */
	@FXML
	private void displayImage() {
		if(this.curPhoto == null) {
			ErrorMsg.Error("ERROR!", "You didn't choose any photo to display!", 
					"Please select a photo and try again!");
			return;
		}
		this.photoStorage.pagePhotoView(curPhoto);
	}
	
	/**
	 * Copy the image to the selected album in Album Selection page
	 *   
	 */
	@FXML 
	private void copyImage() {
		if(curPhoto==null) {
			ErrorMsg.Error("ERROR!", "No photo to copy!", 
					"Please select a photo and try again!");
			return;
		}
		this.photoStorage.pageAlbumSelection(false, curPhoto);
	}
	
	/**
	 * Move the image to the selected album in Album Selection page
	 *   
	 */
	@FXML
	private void moveImage() {
		if(curPhoto==null) {
			ErrorMsg.Error("ERROR!", "No photo to move!", 
					"Please select a photo and try again!");
			return;
		}
		this.photoStorage.pageAlbumSelection(true, curPhoto);
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
		this.photoStorage = photoStorage;
		photos = FXCollections.observableList(photoStorage.getAlbum().getPhotos());
		setGridPane(0);
		this.photoStorage.getPrimaryStage().setTitle("Username: " + this.photoStorage.getUser().getUserName() + ", Album name: " + (photoStorage.getAlbum().getAlbumName()));
	    
	}

}
