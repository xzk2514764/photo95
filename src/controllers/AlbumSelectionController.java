package controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import photostorage.PhotoStorage;
import storage.Album;

/**
 * This class control Album Selection.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class AlbumSelectionController {
	private PhotoStorage photoStorage;
	private boolean mode;//true -> move, false -> copy
	
	@FXML
    private TableView<Album> albumViews;
	@FXML
    private TableColumn<Album, String> albumCols;
	
	/**
	 * Cancel this action and then go to Album View page
	 *   
	 */
	@FXML
	private void cancel () {
		photoStorage.pageAlbumView(photoStorage.getAlbum());
	}
	
	/**
	 * Confirm this action to the selected album
	 *   
	 */
	@FXML
	private void ok() {
		if(photoStorage.getAlbum() == albumViews.getSelectionModel().getSelectedItem()) {
			ErrorMsg.Error("ERROR!", "Cannot move/copy to the original album!", "Please choose another album!");
			return;
		}
		
		if( albumViews.getSelectionModel().getSelectedItem().getPhotos().contains(photoStorage.getPhoto())) {
			ErrorMsg.Error("ERROR!", "Cannot move/copy to the selected album!", "Album already has a copy of this image!");
			return;
		}
		else {
			if(mode) {
				photoStorage.getAlbum().getPhotos().remove(photoStorage.getPhoto());
			}
			albumViews.getSelectionModel().getSelectedItem().getPhotos().add(photoStorage.getPhoto());
		}
		photoStorage.pageAlbumView(photoStorage.getAlbum());
	}
	
	/**
	 * Initialize this fxml stage
	 *   
	 */
	@FXML
	private void initialize() {
		albumCols.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAlbumName()));
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
		this.photoStorage = photoStorage;
		this.mode = photoStorage.cutImage();
		albumViews.setItems(FXCollections.observableList(photoStorage.getUser().getAlbums()));
		albumViews.getSelectionModel().select(0);
	 	 
	}
}
