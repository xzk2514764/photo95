package controllers;

import javafx.scene.control.Alert;

/**
 * This class control Error Message
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class ErrorMsg {
	
	/**
	 * 
	 * Display the error message
	 * 
	 * @param title The title of the error message
	 * @param header The first row - header of the error message
	 * @param content The content - details of the error message
	 */
	public static void Error(String title, String header, String content) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
}
