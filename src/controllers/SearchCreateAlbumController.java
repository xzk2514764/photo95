package controllers;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * This class control SearchCreateAlbum.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class SearchCreateAlbumController {
	private Stage stage;
	private UserSearchController controller;
	
	
	@FXML
	private TextField albumName;
	
	/**
	 * Confirm to create a new album with search result
	 * The album name is the input
	 * 
	 */
	@FXML
	private void yes() {
		String name = albumName.getText().strip();
		if(name.length() == 0) {
			ErrorMsg.Error("ERROR!", "empty album name not allowed!", 
					"Please change the name!");
			return;
		}
		boolean success= controller.setAlbumName(name);
		if(!success) {
			ErrorMsg.Error("ERROR!", "Album name already exists!", 
					"Please change the name!");
			return;
		} else {
			stage.close();
			controller.createAlbum();
		}		
	}
	
	/**
	 * Do not to create a new album
	 * 
	 */
	@FXML
	private void no() {
		stage.close();
		controller.setAlbumName("temporary-search-result ");
		controller.noCreateAlbum();
	}
	
	/**
	 * Set to its father stage
	 * 
	 * @param window The stage
	 * @param c The User Search Controller
	 */
	public void setMainStage(Stage window, UserSearchController c) {
		stage = window;
		controller = c;
		if(controller.setAlbumName("search result")) {
			albumName.setText("search result");
		} else {
			int index = 1;
			while(true) {
				String name = "search result" + index;
				if(controller.setAlbumName(name)) {
					albumName.setText(name);
					break;
				}
			}
		}
		
		
	}
}
