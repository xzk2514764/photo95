package controllers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import photostorage.PhotoStorage;
import storage.Album;
import storage.Photo;
import storage.Tag;
import java.io.File;
import java.util.Date;

/**
 * This class control Photo View.fxml
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class PhotoViewController {
	private PhotoStorage photoStorage;
	private Photo curPhoto; 
	private List<Photo> photos;
	private Album curAlbum;
	private IntegerProperty firstImg = new SimpleIntegerProperty(0);
    private IntegerProperty lastImg = new SimpleIntegerProperty(0);
    private IntegerProperty noTag = new SimpleIntegerProperty(0);
    private IntegerProperty inputNewTag = new SimpleIntegerProperty(0);
    private Map<Photo, ObservableList<Tag>> photo2Tag = new HashMap<Photo, ObservableList<Tag>>();
    private Map<Tag, SimpleStringProperty> tag2values = new HashMap<Tag, SimpleStringProperty>();
    private ObservableList<String> allTagKeys; 
	@FXML
	private Label caption;
	
	@FXML
	private Label date;
	
	@FXML
	private Button prevImg;
	
	@FXML
	private Button nextImg;
	
	@FXML
	private Button deleteTag;
	
	@FXML
	private Button newTagKey;
	
	@FXML
	private Button cancel;
	
	@FXML
	private TextField tagNameInput;
	
	@FXML
	private TextField tagValueInput;

	@FXML
    private TableView<Tag> tagTable;
	
	@FXML
    private TableColumn<Tag, String> nameC;
	
	@FXML
    private TableColumn<Tag, String> valueC;
	
	@FXML
	private ImageView imageDisplay;
	
	@FXML
	private ChoiceBox<String> chooseKey;
	
	/**
	 * Initialize this fxml stage
	 *   
	 */
	@FXML
	private void initialize() {
		nextImg.disableProperty().bind(lastImg.greaterThan(0));
		prevImg.disableProperty().bind(firstImg.greaterThan(0));
		nameC.setCellValueFactory(cellData-> new SimpleStringProperty(cellData.getValue().getKey()));
		valueC.setCellValueFactory(cellData-> this.tag2values.get(cellData.getValue()));
		deleteTag.disableProperty().bind(noTag.greaterThan(0));
		chooseKey.visibleProperty().bind(inputNewTag.lessThan(1));
		tagNameInput.visibleProperty().bind(inputNewTag.greaterThan(0));
		newTagKey.visibleProperty().bind(inputNewTag.lessThan(1));
		cancel.visibleProperty().bind(inputNewTag.greaterThan(0));
	}
	
	/**
	 * Add a tag to current photo
	 * You need to choose a tag key and have a input as the tag
	 * 
	 */
	@FXML
	private void addTag() {
		String tagName = chooseKey.getValue();
		if(tagName == null && inputNewTag.get() < 0) {
			ErrorMsg.Error("ERROR!", "No Tag key Chosen!", 
					"Please choose or add a tag key!");
			return;
		}
		String tagValue = tagValueInput.getText().strip();
		if(tagValue.length() == 0) {
			ErrorMsg.Error("ERROR!", "Empty tag value!", 
					"Please write something to your tag!");
			return;
		}
		if(this.curPhoto.hasTag(tagName, tagValue)) {
			ErrorMsg.Error("ERROR!", "tag already exists!", 
					"Please change to another tag value and try again!");
			return;
		}
		if(inputNewTag.get() > 0) {
			tagName = tagNameInput.getText().strip();
			if(tagName.length() == 0) {
				ErrorMsg.Error("ERROR!", "Empty tag name!", 
						"Please write something!");
				return;
			}
			inputNewTag.set(0);
			boolean alreadyHas = false;
			for(String k: allTagKeys) {
				if(k.equals(tagName)) {
					alreadyHas = true;
					break;
				}
			}
			if(!alreadyHas) {
				allTagKeys.add(tagName);
			}
		}
		
		
		boolean hasKey = false;
		for(Tag tag: this.photo2Tag.get(this.curPhoto)) {
			if(tag.getKey().equals(tagName)) {
				tag.addValue(tagValue);
				hasKey = true;
				this.tag2values.get(tag).set(tag.getValues().toString());
			}
		}
		if(!hasKey) {
			Tag tag = new Tag(tagName,tagValue);
			this.photo2Tag.get(this.curPhoto).add(tag);
			this.tag2values.put(tag, new SimpleStringProperty(tag.getValues().toString()));
		}
		tagTable.getSelectionModel().select(this.photo2Tag.get(this.curPhoto).size() - 1);
		noTag.set(0);
		tagNameInput.clear();
		tagValueInput.clear();
	}
	
	/**
	 * Add a new tag key
	 * This user can use this tag key in future
	 * 
	 */
	@FXML
	private void addNewTagKey() {
		inputNewTag.set(1);
	}
	
	/**
	 * Cancel to add a new tag key
	 * 
	 */
	@FXML
	private void cancel() {
		inputNewTag.set(0);
		tagNameInput.clear();
		tagValueInput.clear();
	}
	
	/**
	 * Delete the selected tag
	 * If this tag have multiple value, then go to popup page to choose which one you want to delete
	 */
	@FXML
	private void deleteTag() {
		int index = tagTable.getSelectionModel().getSelectedIndex();
		Tag tag = this.curPhoto.getAllTags().get(index);
		if(tag.getValues().size() == 1) {
			photo2Tag.get(this.curPhoto).remove(tag);
			if(photo2Tag.get(this.curPhoto).size() == 0) {
				noTag.set(1);
			}
			return;
		}
		Stage newWindow = new Stage();
		newWindow.initModality(Modality.WINDOW_MODAL);
		newWindow.initOwner(this.photoStorage.getPrimaryStage());
		this.photoStorage.addSubStage(newWindow);
		ObservableList<String> obl = FXCollections.observableList(tagTable.getItems().get(index).getValues());
		try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../photostorage/popup.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            newWindow.setScene(new Scene(layout));
            PopupController controller = loader.getController();
            controller.setMainStage(obl, newWindow, index, this.tag2values.get(tag));
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        newWindow.setX(this.photoStorage.getPrimaryStage().getX());
        newWindow.setY(this.photoStorage.getPrimaryStage().getY());

        newWindow.show();

	}
	
	/**
	 * Back and go to Album View page
	 * 
	 */
	@FXML
	private void back() {
		this.photoStorage.pageAlbumView(curAlbum);
	}
	
	/**
	 * Logout and then go to login page
	 *   
	 */
	@FXML
	private void logout() {
		this.photoStorage.pageLogin();
	}
	
	/**
	 * View previous one image in this album sequence
	 *   
	 */
	@FXML
	private void prevImg() {
		int index = this.photos.indexOf(this.curPhoto);
		index --;
		if(index == 0) {
			this.firstImg.set(1);
		}
		this.lastImg.set(0);
		this.curPhoto = this.photos.get(index);
		setNewImg();
	}
	
	/**
	 * View next one image in this album sequence
	 *   
	 */
	@FXML
	private void nextImg() {
		int index = this.photos.indexOf(this.curPhoto);
		index ++;
		if(index == this.photos.size() - 1) {
			this.lastImg.set(1);
		}
		this.firstImg.set(0);
		this.curPhoto = this.photos.get(index);
		setNewImg();
	}
	
	/**
	 * Set the new image
	 *   
	 */
	private void setNewImg() {
		File file = new File(this.curPhoto.getPath());
		this.imageDisplay.setImage(new Image(file.toURI().toString()));
		this.imageDisplay.setPreserveRatio(true);
		this.caption.setText(this.curPhoto.getCaption());
		this.date.setText(""+new Date(new File(this.curPhoto.getPath()).lastModified()));
		if(photo2Tag.get(this.curPhoto) == null) {
			this.photo2Tag.put(this.curPhoto, FXCollections.observableList(this.curPhoto.getAllTags()));
		}
		this.tagTable.setItems(this.photo2Tag.get(this.curPhoto));
		if(this.curPhoto.getAllTags().size() == 0) {
			this.noTag.set(1);
		} else {
			this.tagTable.getSelectionModel().select(0);
			this.noTag.set(0);
		}
		this.tag2values =  new HashMap<Tag, SimpleStringProperty>();
		for(Tag tag: this.curPhoto.getAllTags()) {
			tag2values.put(tag, new SimpleStringProperty(tag.getValues().toString()));
		}
	}
	
	/**
	 * Set to the main photoStorage
	 * 
	 * @param photoStorage The photo storage
	 */
	public void setMainApp(PhotoStorage photoStorage) {
		this.photoStorage = photoStorage;
		this.curPhoto = photoStorage.getPhoto();
		this.curAlbum = photoStorage.getAlbum();
		this.photos = photoStorage.getAlbum().getPhotos();
		File file = new File(this.curPhoto.getPath());
		this.imageDisplay.setImage(new Image(file.toURI().toString()));
		this.imageDisplay.setPreserveRatio(true);
		this.caption.setText(this.curPhoto.getCaption());
		this.date.setText(""+new Date(new File(this.curPhoto.getPath()).lastModified()));
		this.photo2Tag.put(this.curPhoto, FXCollections.observableList(this.curPhoto.getAllTags()));
		this.tag2values =  new HashMap<Tag, SimpleStringProperty>();
		for(Tag tag: this.curPhoto.getAllTags()) {
			tag2values.put(tag, new SimpleStringProperty(tag.getValues().toString()));
		}
		this.tagTable.setItems(this.photo2Tag.get(this.curPhoto));
		int index = this.photos.indexOf(curPhoto);
		if(index == 0) {
			this.firstImg.set(1);
		}
		if(index == this.photos.size() - 1) {
			this.lastImg.set(1);
		}
		
		if(this.curPhoto.getAllTags().size() == 0) {
			this.noTag.set(1);
		}
		
		if(this.curPhoto.getAllTags().size() > 0) {
			this.tagTable.getSelectionModel().select(0);
		}
		allTagKeys = FXCollections.observableList(this.photoStorage.getUser().getAllTagKeyTypes());
		this.chooseKey.setItems(allTagKeys);
	}
}
