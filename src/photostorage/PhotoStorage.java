package photostorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import controllers.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import storage.*;

/**
 * This class is the main method of photo storage project
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class PhotoStorage extends Application {
	private final static String USERFILENAME = "/userstorage";
    private Stage primaryStage;
    private ObservableList<User> users = FXCollections.observableList(new ArrayList<>());
    private User curUser;
    private Album curAlbum;
    private Photo curPhoto;
    private List<Stage> subStages = new ArrayList<Stage>();
    private boolean cutImg;
    private boolean removePersistence = false;
    
    /**
     * Start this program
     * 
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        users.add(new User("admin"));
        readUsersFromFile();
        pageLogin();
        primaryStage.show();
    }

    /**
     * Get the Primary Stage
     * 
     * @return the Primary Stage
     */
    public Stage getPrimaryStage() {
    	return primaryStage;
    }
    
    /**
     * Add the new stage to the subStages
     * 
     * @param s The stage that we want to add
     */
    public void addSubStage(Stage s) {
    	this.subStages.add(s);
    }
    
    /**
     * Go to the login.fxml
     * 
     */
    public void pageLogin() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("login.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            LoginController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the admin.fxml
     * 
     */
    public void pageAdmin() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("admin.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            AdminController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the User Albums.fxml with current user name
     * 
     * @param username Current user name
     */
    public void pageUserAlbums(String username) {
        try {
        	setUser(username);
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("User Albums.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            UserAlbumsController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the User Albums.fxml
     * 
     */
    public void pageUserAlbums() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("User Albums.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            UserAlbumsController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the Album View.fxml with current album name
     * 
     * @param album Current album name
     */
    public void pageAlbumView(Album album) {
        try {
        	this.curAlbum = album;
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Album View.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            AlbumViewController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the Photo View.fxml with current photo
     * 
     * @param photo Current photo
     */
    public void pagePhotoView(Photo photo) {
        try {
        	this.curPhoto = photo;
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Photo View.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            PhotoViewController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the Album Selection.fxml with current photo and copy or move boolean
     * 
     * @param cut Copy or Move boolean
     * @param curPhoto Current photo
     */
    public void pageAlbumSelection(boolean cut, Photo curPhoto) {
        try {
        	this.cutImg = cut;
        	this.curPhoto = curPhoto;
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Album Selection.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            AlbumSelectionController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Go to the User Search.fxml
     * 
     */
    public void pageUserSearch() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("User Search.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();

            primaryStage.setScene(new Scene(layout));
            UserSearchController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Get the boolean cutImg
     * 
     * @return Boolean cutImg
     */
    public boolean cutImage() {
    	return this.cutImg;
    }
    
    /**
     * Get the current album
     * 
     * @return Current album
     */
    public Album getAlbum() {
    	return this.curAlbum;
    }
    
    /**
     * Get the current photo
     * 
     * @return Current photo
     */
    public Photo getPhoto() {
    	return this.curPhoto;
    }
    
    /**
     * Get the current user
     * 
     * @return Current user
     */
    public User getUser() {
    	return this.curUser;
    }
    
    /**
     * Set the selected user with the input user name
     * 
     * @param username Input user name
     */
    private void setUser(String username) {
    	for(User user: users) {
    		if (user.getUserName().equals(username)) {
    			this.curUser = user;
    			return;
    		}
    	}
    }
    
    /**
     * Get the list of users
     * 
     * @return The list of users
     */
    public List<User> getUsers() {
    	return new ArrayList<User>(this.users);
    }
    
    /**
     * Get the list of modifiable users
     * 
     * @return The list of modifiable users
     */
    public ObservableList<User> getModifiableUsers() {
    	return this.users;
    }
    
    /**
     * Save the change to file
     * 
     */
    private void writeUsersToFile() {
		try {
			Calendar calendar = Calendar.getInstance();
			long timeLong = calendar.getTimeInMillis();
			File root = new File(USERFILENAME);
			if(!root.exists()) {
				root.mkdir();
			}
			String fileName = USERFILENAME + "/" + timeLong;
			File dir = new File(fileName);
			if(dir.list()!=null) {
				for(String u:dir.list()) {
					File fx = new File(fileName+"/"+u);
					fx.delete();
				}
			}
			
			dir.delete();
			dir.mkdir();
			while(root.list().length > 10) {
				long min = 0;
				for(String version:root.list()) {
					long time = Long.parseLong(version);
					if(min == 0 ||  time < min) {
						min = time;
					}
				}
				
				File outdatedFile = new File(USERFILENAME + "/" + min);
				outdatedFile.delete();
			}
			
			ObjectOutputStream oos;
			for (int i = 1; i < this.users.size(); i++) {
				String filePath = fileName + File.separator  + i;
				File file = new File(filePath);
				file.createNewFile();
				OutputStream out = new FileOutputStream(filePath);
				oos = new ObjectOutputStream(out);
				oos.writeObject(users.get(i));
				oos.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    /**
     * Add the stock photos
     * 
     */
    private void addAllStockPhotos() {
    	User user = new User("stock");
    	boolean hasStock = false;
    	for(User u:this.users) {
    		if(u.getUserName().equals("stock")) {
    			user = u;
    			hasStock = true;
    		}
    	}
    	if(!hasStock) {
    		this.users.add(user);
    	}
    	boolean hasStockAlbum = false;
    	Album album = new Album("stock");
    	for(Album a: user.getAlbums()) {
    		if(a.getAlbumName().equals("stock")) {
    			album = a;
    			hasStockAlbum = true;
    		}
    	}
    	if(!hasStockAlbum) {
    		user.getAlbums().add(album);
    	}
		album.getPhotos().clear();
		File stockDir = new File("./data");
		for(String photoName: stockDir.list()) {
			Photo p = new Photo("./data/" + photoName);
			user.getAlbums().get(0).getPhotos().add(p);
		}
    }
    
    /**
     * Reset this program
     * 
     */
    public void resetProgram() {	
    	this.users.clear();
    	users.add(new User("admin"));
    	addAllStockPhotos();
    }
    
    /**
     * Remove all persistent files
     * 
     */
    public void removeAllPersistentFiles() {
    	File root = new File(USERFILENAME);
    	if(root.list()!=null) {
    		for(String file: root.list()) {
    			File d = new File(USERFILENAME + "/" + file);
    			if(d.list()!=null) {
    				for(String  u: d.list()) {
    					File f = new File((USERFILENAME + "/" + file + "/"+u));
    					f.delete();
    				}
    			}
    			d.delete();	
        	}
    	}
    	root.delete();
    	removePersistence = true;
    	primaryStage.close();
    	System.exit(0);
    }
        
    /**
     * Get all version of programs
     * 
     * @return All versions of programs
     */
    public List<Date> getAllVersion() {
    	File root = new File(USERFILENAME);
    	List<Date> d = new ArrayList<>();
    	if(root.list()!=null) {
    		for(String v: root.list()) {
        		d.add(new Date( Long.parseLong(v)));
        	}
    	}
    	
    	return d;
    }
    
    /**
     * Rebase the version of programs
     * 
     * @param version The old version
     */
    public void rebaseVersion(long version) {
    	this.users.clear();
    	users.add(new User("admin"));
    	try {
			File root = new File(USERFILENAME);
			if (!root.exists()) {
				addAllStockPhotos();
				return;
			}

			ObjectInputStream ois;
			File dir = new File(USERFILENAME + "/" + version);
			for(String filename: dir.list()) {
				File file = new File(USERFILENAME + "/" + version + File.separator + filename);
				InputStream input = new FileInputStream(file);
				ois = new ObjectInputStream(input);
				User user = (User) ois.readObject();
				this.users.add(user);
				ois.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Read the information from file
     * 
     */
    private void readUsersFromFile() {
    	try {
			File root = new File(USERFILENAME);
			if (!root.exists() || root.list().length == 0) {
				addAllStockPhotos();
				return;
			}

			ObjectInputStream ois;
			long max = 0;
			for(String version:root.list()) {
				long time = Long.parseLong(version);
				if(time > max) {
					max = time;
				}
			}
			File dir = new File(USERFILENAME + "/" + max);
			for(String filename: dir.list()) {
				File file = new File(USERFILENAME + "/" + max + File.separator + filename);
				InputStream input = new FileInputStream(file);
				ois = new ObjectInputStream(input);
				User user = (User) ois.readObject();
				this.users.add(user);
				ois.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Save the file and close the subStages
     * It will be used when we stop the program
     * 
     */
    @Override
    public void stop(){
    	if(removePersistence) {
    		return;
    	}
    	writeUsersToFile();
    	for(Stage subStage:this.subStages) {
    		subStage.close();
    	}
    }
    
    /**
	 * The main method that we start the chess game loop.
	 * If one player win the game, the game will end.
	 * @param args The command-line arguments
	 */
    public static void main(String[] args) {
        launch(args);
    }
}