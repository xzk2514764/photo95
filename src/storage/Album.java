package storage;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is to define Album
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class Album implements Serializable {
	static final long serialVersionUID = 1L;
	private String name;
	private List<Photo> photos = new ArrayList<>();
	
	/**
     * The constructor of album
     * 
     * @param name The album name
     */
	public Album(String name) {
		this.name = name;
	}
	
	/**
	 * Get the album name
	 * 
	 * @return The String of the album name
	 */
	public String getAlbumName() {
		return this.name;
	}
	
	/**
	 * Change the album name with the new album name
	 * 
	 * @param name The new album name
	 */
	public void changeAlbumName(String name) {
		this.name = name;
	}
	
	/**
	 * Get the list of photos of this album
	 * 
	 * @return The list of photos
	 */
	public List<Photo> getPhotos() {
		return this.photos;
	}
	
	/**
	 * Get the album name with format
	 * 
	 * @return "albumname:" + The String of the album name
	 */
	@Override
	public String toString() {
		String result = "albumname:" + name + "\n";
		for(Photo photo:photos) {
			result += photo.toString();
		}
		return result;
	}
}
