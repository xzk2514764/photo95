package storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is to define User
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class User implements Serializable {
	static final long serialVersionUID = 1L;
	static final String[] DEFAULT_TAG = new String[]{"person", "location"};
	private String userName;
	private List<Album> albums = new ArrayList<Album>();
	private List<String> tagKeyTypes = new ArrayList<String>(Arrays.asList(DEFAULT_TAG));
	
	/**
	 * The constructor of User
	 * 
	 * @param userName The user name
	 */
	public User(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Get the user name
	 * 
	 * @return The user name
	 */
	public String getUserName() {
		return this.userName;
	}
	
	/**
	 * Get the list of albums of this user
	 * 
	 * @return The list of albums
	 */
	public List<Album> getAlbums() {
		return this.albums;
	}
	
	/**
	 * Get the list of tag keys of this user
	 * 
	 * @return The list of tag keys
	 */
	public List<String> getAllTagKeyTypes() {
		return tagKeyTypes;
	}

	/**
	 * Get the user name with format
	 * 
	 * @return "username:" + The String of the user name
	 */
	@Override
	public String toString() {
		String result = "username:"+this.userName +"\n";
		for(Album album: albums) {
			result += album.toString();
		}
		return result;
	}
	
}
