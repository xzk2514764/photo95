package storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is to define Tag
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class Tag implements Serializable {
	static final long serialVersionUID = 1L;
	private String key;
	private List<String> value = new ArrayList<>();
	
	/**
	 * The constructor of tag 
	 * @param key The tag key
	 * @param value The tag value
	 */
	public Tag(String key, String value) {
		this.key = key;
		this.value.add(value);
	}
	
	/**
	 * Add tag value
	 * @param v2 The tag value that we want to add
	 * @return True if add, false if not add because the tag is already exist
	 */
	public boolean addValue(String v2) {
		for(String v: value) {
			if(v.equals(v2)) {
				return false;
			}
		}
		value.add(v2);
		return true;
	}
	
	/**
	 * Get the tag key
	 * 
	 * @return The tag key
	 */
	public String getKey() {
		return this.key;
	}
	
	/**
	 * Get the list of tag value
	 * 
	 * @return The list of tag value
	 */
	public List<String> getValues() {
		return this.value;
	}
	
	/**
	 * Get the tag key and value with format
	 * 
	 * @return "tag:" + The String of the tag key and value
	 */
	@Override
	public String toString() {
		return "tag:" + key +"\n" + value + "\n";
	}
}