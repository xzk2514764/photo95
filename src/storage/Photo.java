package storage;

import java.util.List;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is to define Photo
 * 
 * @author Haoran Yu
 * @author Zikang Xia
 * 
 */
public class Photo implements Serializable {
	static final long serialVersionUID = 1L;
	public static String THUNBNAILIMAGE = "thumbnail images";
	private String caption = "";
	private String path;
	private List<Tag> tags = new ArrayList<>();
	
	/**
	 * The constructor of photo
	 * 
	 * @param path The path of photo
	 */
	public Photo(String path) {
		this.path = path;
		File file = new File(path);
		caption = file.getName();
	}
	
	/**
	 * Get the get thumb nail of this photo
	 * 
	 * @return The image
	 * @throws IOException Throw IO exception
	 */
	public Image getThumbnail() throws IOException{
		File file = new File(path);
		return ImageIO.read(file).getScaledInstance(100, 100, Image.SCALE_SMOOTH);
	}
	
	/**
	 * Set the caption to this.caption
	 * 
	 * @param caption The caption of photo
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * Get the photo caption of this photo
	 * 
	 * @return The photo caption
	 */
	public String getCaption() {
		return this.caption;
	}
	
	/**
	 * Check if this photo is valid
	 * 
	 * @return True if valid, false if not valid
	 */
	public boolean isValid() {
		File file = new File(this.path);
		if (!file.exists()) {
			return false;
		}
		try {
			if( ImageIO.read(file) == null) {
				return false;
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Get the path of this photo
	 * 
	 * @return The path of photo
	 */
	public String getPath() {
		return this.path;
	}
	
	/**
	 * Get the last modified time
	 * 
	 * @return The time of last modified
	 */
	public long getLastModifiedTime() {
		return new File(path).lastModified()/1000;
	}
	
	/**
	 * Check if this photo have tags
	 * 
	 * @param key The tag key
	 * @param value The tag value
	 * @return True if have tags, false if not have
	 */
	public boolean hasTag(String key, String value) {
		for(Tag thisTag: this.tags) {
			if(thisTag.getKey().equals(key)) {
				for(String v: thisTag.getValues()) {
					if(v.equals(value)){
						return true;
					}
				}
				break;
			}
		}
		return false;
	}
	
	/**
	 * Get the list of tags of this photo
	 * 
	 * @return The list of tags
	 */
	public List<Tag> getAllTags() {
		return this.tags;
	}
	
	/**
	 * Get the photo caption with format
	 * 
	 * @return "photoname:" + The String of the photo caption
	 */
	@Override
	public String toString() {
		String result = "photoname:" + this.path + "\n" + this.caption + "\n";
		for(Tag tag:tags) {
			result += tag.toString();
		}
		return result;
	}
}
